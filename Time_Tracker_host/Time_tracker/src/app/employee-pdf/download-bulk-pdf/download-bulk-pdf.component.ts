import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DownloadEmployeePdfComponent } from 'app/employee-list/download-employee-pdf/download-employee-pdf.component';
import { EmployeeDataPDF } from 'app/employee-list/employee-list.component';
import { DataService } from 'app/service/data.service';
import { EmployeeBulkPDF } from '../employee-pdf.component';

class EmployeePDF {
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  address: String;
  birthdate: any;
  work_mode: String;

  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;

  monthPDF: String;
  monthStringPDF: String;
  dayPDF: String;
}

@Component({
  selector: 'download-bulk-pdf',
  templateUrl: './download-bulk-pdf.component.html',
  styleUrls: ['./download-bulk-pdf.component.css']
})
export class DownloadBulkPdfComponent implements OnInit {

  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  dayPDF: String;
  monthPDF: String;

  @Output() downloadPDF = new EventEmitter<any>();
  
  employeePDFList: any;

  constructor(
    public dialogRef: MatDialogRef<DownloadEmployeePdfComponent>,
    public _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
    this.employeePDFList = this.data;
  }

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  submit(){
    let arr: EmployeePDF[] = [];

    this.employeePDFList.forEach(element => {
      arr = element;
    });
    
    let monthString = this.months[Number(this.monthPDF)-1];
    let month = this.monthPDF;
    let day = this.dayPDF;

    arr.forEach(function(val){
      val.name = val.firstName + " " + val.lastName;
      val.monthStringPDF = monthString
      val.monthPDF = month
      val.dayPDF = day;
    });
    
    this.downloadPDF.emit(arr);
    this.dialogRef.close();
  }

}
