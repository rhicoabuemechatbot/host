import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadBulkPdfComponent } from './download-bulk-pdf.component';

describe('DownloadBulkPdfComponent', () => {
  let component: DownloadBulkPdfComponent;
  let fixture: ComponentFixture<DownloadBulkPdfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadBulkPdfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadBulkPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
