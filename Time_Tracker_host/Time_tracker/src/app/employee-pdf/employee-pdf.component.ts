import { DatePipe } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AddEmployeeDialogComponent } from 'app/employee-list/add-employee-dialog/add-employee-dialog.component';
import { DeleteEmployeeDialogComponent } from 'app/employee-list/delete-employee-dialog/delete-employee-dialog.component';
import { DownloadEmployeePdfComponent } from 'app/employee-list/download-employee-pdf/download-employee-pdf.component';
import { UpdateEmployeeDialogComponent } from 'app/employee-list/update-employee-dialog/update-employee-dialog.component';
import { DataService } from 'app/service/data.service';
import * as JSZip from 'jszip';
import { map } from 'rxjs/operators';
import { DownloadBulkPdfComponent } from './download-bulk-pdf/download-bulk-pdf.component';
import { saveAs } from 'file-saver';
import { ZoomControlStyle } from '@agm/core';

export interface EmployeeData {
  employee: Employee;
}

export interface EmployeeDataPDF {
  employee: EmployeePDF;
}

export interface EmployeeBulkPDF{
  employeeBulk: EmployeePDF[];
}

class Employee {
  id: String;
  
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  designation_name: String;
  cluster_name: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  work_mode: String;
  address: String;
  birthdate: any;
  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

class TimeTrack{
  id: String;
  name: String;
  employee_id: String;
  morning_time_in: String;
  updated_time: String;
  work_mode: String;
}

class EmployeePDF{
  id: String;
  
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  designation_name: String;
  cluster_name: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  work_mode: String;
  address: String;
  birthdate: any;
  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;

  monthPDF: String;
  monthStringPDF: String;
  dayPDF: String;
}

class EmployeeName{
  name = '';
}

@Component({
  selector: 'employee-pdf',
  templateUrl: './employee-pdf.component.html',
  styleUrls: ['./employee-pdf.component.css']
})

export class EmployeePdfComponent implements OnInit {

  // employeesList: any;
  employeesList: EmployeePDF[] = [];

  listBlob = [];
  all_result: Number;

  employee = new Employee();
  currentUser = new Employee();
  employeePDF = new EmployeePDF();
  employeeName = new EmployeeName();

  timetrack = new TimeTrack();

  clusters: any;

  constructor(
    private _dataService: DataService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    public datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.getInitialEmployees();
    this.getCurrentUser();
    this.getAllClusters();
  }

  getInitialEmployees(){
    this._dataService.getEmployees()
      .subscribe((res: EmployeePDF[]) => {
        this.employeesList = res as EmployeePDF[];
        console.log(typeof(res));
    });
  }

  getSelectedEmployees(){
    this._dataService.getSelectedEmployees(this.employeeName)
      .subscribe((res) => {
        this.employeesList = res as EmployeePDF[];
    });
  }

  getAllClusters(){
    this._dataService.getAllClusters()
      .subscribe((res) => {
        this.clusters = res;
    });
  }

  getCurrentUser(){
    this._dataService.getCurrentUser()
      .subscribe((res) => {
        this.currentUser = res['employee'];
        this.currentUser.name = this.currentUser.firstName + " " + this.currentUser.lastName;
    }); 
  }

  changeCluster(cluster: any){
    this._dataService.getEmployees().subscribe((data) => {
      var dataMap = <Array<any>> data;

      var employee_cluster = [];
      dataMap.forEach(function (value){
        if(value['cluster_name'] == cluster){
          employee_cluster.push(value);
        }
      });

      this.employeesList = employee_cluster;
    });
  }

  //CRUD employee
  addEmployee(): void {
    const dialogRef = this.dialog.open(AddEmployeeDialogComponent, {
      width: '1000px',
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.submitClicked.subscribe(result => {
        this._dataService.createEmployees(result)
          .subscribe((res) => {
            console.log(res);
            
            this.openSnackBar("Success! Completely Registered");
            this.getInitialEmployees();
        }, (error) => {
          let error_message = error['error']['message'];
          this.openSnackBar(error_message);
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  updateEmployee(employee: Employee): void {
    const dialogRef = this.dialog.open(UpdateEmployeeDialogComponent, {
      width: '1000px',
      data: {employee: employee}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.updateClicked.subscribe(result => {
        this._dataService.updateEmployee(result)
          .subscribe((res) => {
            console.log(res);
            this.openSnackBar('Successfully updated user!');
          }, (error) => {
            console.log(error);
            this.openSnackBar(error);
          });
        dialogSubmitSubscription.unsubscribe();
    });
  } 
  deleteEmployee(id: Employee){
    const dialogRef = this.dialog.open(DeleteEmployeeDialogComponent, {
      width: '500px',
      data: {employee: id}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.deleteClicked.subscribe(result => {
        
        this._dataService.deleteEmployee(result)
          .subscribe((res) => {
            console.log(res);
            this.openSnackBar("Employee successfully deleted!");
            this.getInitialEmployees();
        }, (error) => {
          this.openSnackBar(error);
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  //use this for downloading time track pdf
  downloadPDF(employee: EmployeePDF){
    employee.name = employee.firstName + " " + employee.lastName;
  
    const dialogRef = this.dialog.open(DownloadEmployeePdfComponent, {
      width: '500px',
      data: {employee: employee}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.downloadPDF.subscribe(result => {
        console.log(result);

        this._dataService.downloadPDF(result)
          .subscribe((res) => {
            this.saveAsBlob(res);
            console.log(res);
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }

  downloadBulkPDF(){
    this._dataService.downloadBulkPDF<EmployeePDF>(this.employeesList)
      .pipe(
        map(value => [value])
      )
      .subscribe(result => {
        const dialogRef = this.dialog.open(DownloadBulkPdfComponent, {
          width: '500px',
          data: result
        });
    
        dialogRef.afterClosed().subscribe(result => {});
    
        const dialogSubmitSubscription = 
          dialogRef.componentInstance.downloadPDF.subscribe(result => {
            this.all_result = result.length;

            result.forEach(element => { 
              console.log(element);
              this._dataService.downloadPDF(element)
              .subscribe((res) => {
                // this.saveAsBlob(res);
                // this.listBlob.push(res);
                // this.saveZip(res);
                this.listBlob.push(res);
              });
            });
            
            let interval = setInterval(() => {
              if(this.listBlob.length == this.all_result){
                this.saveZip();
                clearInterval(interval);
              }
            }, 100);

            // this.listBlob = list;
            // console.log('hey!');
            // this.saveZip();

            dialogSubmitSubscription.unsubscribe();
        });
      });
  }

  //time in functionality
  createTime(){
    var now = new Date();
    let latest_date =this.datepipe.transform(now, 'YYYY-MM-dd hh:mm:ss');

    this.timetrack.employee_id = this.currentUser.id;
    this.timetrack.name = this.currentUser.firstName + " " + this.currentUser.lastName;
    this.timetrack.morning_time_in = latest_date;
    this.timetrack.work_mode = this.currentUser.work_mode;

    console.log(this.timetrack );

    this._dataService.timein(this.timetrack)
      .subscribe((res) => {
        this.timetrack.id = res['id'];
        console.log(res);
      });
  }
  updateTime(){
    var now = new Date();
    let latest_date = this.datepipe.transform(now, 'YYYY-MM-dd HH:mm:ss');

    console.log(latest_date);
    this.timetrack.updated_time = latest_date;  

    this._dataService.updateTime(this.timetrack)
      .subscribe((res) => {
        console.log(res);
    });
  }
  overtime(){
    var now = new Date();
    let latest_date =this.datepipe.transform(now, 'YYYY-MM-dd hh:mm:ss');

    this.timetrack.updated_time = latest_date;

    this._dataService.overtime(this.timetrack)
      .subscribe((res) => {
        console.log(res);
    });
  }

  saveAsBlob(data: any){
    var blob = new Blob([data], { type: 'application/pdf' });
    var url= window.URL.createObjectURL(blob);
    setTimeout(() => window.open(url), 1000);
  }

  saveZip(){
    var zip = new JSZip();
    let date = new Date();
    let now = date.getMonth().toString()+"-"+date.getDate().toString()+"-"+date.getFullYear().toString();

    let pdfList = [];

    this.listBlob.forEach(function(element, i){
      console.log(element);
      zip.folder(now.toString()).file("Employee "+i+1+".pdf", element)
      pdfList.push(i);
    });

    let interval = setInterval(() => {
      if(pdfList.length == this.listBlob.length){
        zip.generateAsync({type: "blob"}).then(function(blob){
          saveAs(blob, now.toString()+".zip");
        });
        clearInterval(interval);
      }
    }, 100);
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "Close", { duration: 2000});
  }
}
