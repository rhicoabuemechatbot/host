import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePdfComponent } from './employee-pdf.component';

describe('EmployeePdfComponent', () => {
  let component: EmployeePdfComponent;
  let fixture: ComponentFixture<EmployeePdfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmployeePdfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
