import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/service/auth_service/auth.service';
import { DataService } from 'app/service/data.service';
import { CookieService } from 'ngx-cookie-service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const isAdminROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'dashboard', class: ''},
    // { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
    { path: '/employee-list', title: 'Employee List',  icon:'content_paste', class: '' },
    // { path: '/typography', title: 'Typography',  icon:'library_books', class: '' },
    { path: '/employee-pdf', title: 'Download Employee PDF',  icon:'picture_as_pdf', class: '' },
    { path: '/references', title: 'References',  icon:'book', class: '' },
    { path: '/about-us', title: 'About Us',  icon:'info', class: '' },
    // { path: '/notifications', title: 'Notifications',  icon:'notifications', class: '' },
];

export const employeeROUTES: RouteInfo[] = [
  { path: '/user-profile', title: 'User Profile',  icon:'person', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(
    private _authService: AuthService,
    private _dataService: DataService,
    private _cookieService: CookieService,
  ) { }

  ngOnInit() {
    let isAdmin = this._cookieService.get("isAdmin").toString();

    if(isAdmin == 'true'){
      this.menuItems = isAdminROUTES.filter(menuItem => menuItem);
    } else {
      this.menuItems = employeeROUTES.filter(menuItem => menuItem);
    }
  }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };
}
