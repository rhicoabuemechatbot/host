import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { LoginComponent } from 'app/login/login.component';
import { AuthGuard } from 'app/auth_guard/auth.guard';
import { ReferencesComponent } from 'app/references/references.component';
import { EmployeeListComponent } from 'app/employee-list/employee-list.component';
import { EmployeePdfComponent } from 'app/employee-pdf/employee-pdf.component';
import { AboutUsComponent } from 'app/about-us/about-us.component';

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'login',          component: LoginComponent },
    { 
        path: 'dashboard',      
        component: DashboardComponent,
        canActivate: [AuthGuard], 
    },
    { path: 'user-profile',   component: UserProfileComponent },
    { path: 'employee-list',     component: EmployeeListComponent, canActivate: [AuthGuard],  },
    { path: 'employee-pdf',     component: EmployeePdfComponent, canActivate: [AuthGuard],  },
    { path: 'typography',     component: TypographyComponent, canActivate: [AuthGuard],  },
    { 
        path: 'icons',          
        component: IconsComponent, 
        canActivate: [AuthGuard],  
    },
    { 
        path: 'references',          
        component: ReferencesComponent, 
        canActivate: [AuthGuard],  
    },
    { 
        path: 'about-us',          
        component: AboutUsComponent,   
    },
    { path: 'notifications',  component: NotificationsComponent, canActivate: [AuthGuard],  },
    { path: 'upgrade',        component: UpgradeComponent }
];
