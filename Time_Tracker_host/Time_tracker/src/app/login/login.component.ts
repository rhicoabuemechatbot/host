import { AutofillMonitor } from '@angular/cdk/text-field';
import { AfterViewInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/service/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from 'app/service/auth_service/auth.service';

class User{
  email: String;
  password: String;
}

class UsernameOrEmail{
  username: String;
}

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit,  AfterViewInit, OnDestroy {
  @ViewChild('first', {read: ElementRef}) firstName: ElementRef<HTMLElement>;
  @ViewChild('last', {read: ElementRef}) lastName: ElementRef<HTMLElement>;
  firstNameAutofilled: boolean;
  lastNameAutofilled: boolean;

  user = new User();
  employees: any;

  usernameOrEmail = new UsernameOrEmail();

  isLoading = false;
  email: any;

  constructor(
    private _autofill: AutofillMonitor,
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private _cookieService: CookieService,
    private _authService: AuthService,
  ) {}

  ngAfterViewInit() {
    this._autofill.monitor(this.firstName)
        .subscribe(e => this.firstNameAutofilled = e.isAutofilled);
    this._autofill.monitor(this.lastName)
        .subscribe(e => this.lastNameAutofilled = e.isAutofilled);
  }

  ngOnDestroy() {
    this._autofill.stopMonitoring(this.firstName);
    this._autofill.stopMonitoring(this.lastName);
  }

  ngOnInit(): void {
    this.getData();

    var accessToken = this._cookieService.get("Auth_Token");
    
    if(accessToken.length != 0){
      this.router.navigateByUrl('/dashboard');
    }
  }

  getData(){
    this._dataService.getEmployees().subscribe((res) => {});
  }

  login(){
    this.isLoading = true;

    if(this.usernameOrEmail.username == undefined){
      this.isLoading = false;
    }

    if(!this.usernameOrEmail.username.includes('@')){
      this.findEmailByUsername();
    }
    else{
      this.user.email = this.usernameOrEmail.username;
      this.loginUser();
    }
  }

  findEmailByUsername(){
    this._dataService.findEmailByUsername(this.usernameOrEmail)
      .subscribe((res) => {
        this.user.email = res.toString();

        this.loginUser();
    }, (error) => {
      this.isLoading = false;
      this.openSnackBar("Try again! No account with username: "+this.usernameOrEmail.username);
    });
  }

  loginUser(){
    this._dataService.loginUser(this.user)
      .subscribe(async (res) => {
        this.isLoading = false;

        this.openSnackBar("Successfully logged in!");

        if(res['access_token']){
          this._cookieService.set("Auth_Token", res['access_token']);

          this._dataService.getCurrentUser()
            .subscribe((res) => {
              let isAdmin = res['isAdmin'];
              this._cookieService.set("isAdmin", isAdmin);

              if(isAdmin){
                this.router.navigateByUrl('/dashboard');
              } else {
                this.router.navigateByUrl('/about-us');
              }
            });
        }

    }, (error) => {
      this.isLoading = false;

      this.openSnackBar("Try again! Wrong username/password.");
    });
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "Close", { duration: 2000});
  }
  
}
