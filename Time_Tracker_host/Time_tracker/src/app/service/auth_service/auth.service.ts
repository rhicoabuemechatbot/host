import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import jwt_decode from "jwt-decode";
import { DataService } from '../data.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAdmin: boolean;

  constructor(
    private _cookieService: CookieService,
    private _dataService: DataService,
  ) { }

  loggedIn(){
    var accesstoken = this._cookieService.get("Auth_Token");
    
    if(accesstoken){
      return true; 
    }

    return false;
  }
}
