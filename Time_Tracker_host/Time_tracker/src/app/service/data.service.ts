import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

// @Injectable({
//   providedIn: 'root'
// })

class EmployeePDF{
  id: String;
  
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  designation_name: String;
  cluster_name: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  work_mode: String;
  address: String;
  birthdate: any;
  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;

  monthPDF: String;
  monthStringPDF: String;
  dayPDF: String;
}

@Injectable()
export class DataService {

  constructor(
    private httpClient: HttpClient,
    private _cookieService: CookieService
  ) { }

  baseUrl = 'http://122.53.98.116:8000/api';

  //employees
  getEmployees(){
    return this.httpClient.get(this.baseUrl + '/employee/getEmployees');
  }
  getSelectedEmployees(name: any){
    return this.httpClient.post(this.baseUrl + '/employee/getSelectedEmployees', name);
  }
  createEmployees(employee: any){
    return this.httpClient.post(this.baseUrl + '/employee/createEmployees', employee);
  }
  updateEmployee(employee: any){
    return this.httpClient.put(this.baseUrl + '/employee/updateEmployee', employee);
  }
  deleteEmployee(id: any){
    return this.httpClient.post(this.baseUrl + '/employee/deleteEmployee', id);
  }

  //downloadPDF
  downloadPDF(data: any){
    return this.httpClient.post(this.baseUrl + '/employee/downloadPDF', data, { responseType: 'blob' });
  }

  downloadBulkPDF<EmployeePDF>(data: any){
    return this.httpClient.post(this.baseUrl + '/employee/downloadBulkPDF', data);
  }

  //user
  updateUser(id: number, employee_id: String){
    return this.httpClient.put(this.baseUrl + '/user/updateUser/' + id, employee_id)
  }
  findEmailByUsername(username: any){
    return this.httpClient.post(this.baseUrl+'/user/findEmailByUsername', username);
  }
  loginUser(user: any){
    return this.httpClient.post(this.baseUrl + '/auth/login', user);
  }
  getCurrentUser(){
    var accesstoken = this._cookieService.get("Auth_Token");
    const headerAccessToken = {
      'Authorization': 'Bearer '+accesstoken
    };
    const requestHeader = {
      headers: new HttpHeaders(headerAccessToken)
    };

    return this.httpClient.get(this.baseUrl + '/auth/user-profile', requestHeader);
  }

  //designation
  getAllDesignations(){
    return this.httpClient.get(this.baseUrl + '/employee/getDesignations');
  }
  createDesignations(designation: any){
    return this.httpClient.post(this.baseUrl + '/employee/createDesignation', designation);
  }
  updateDesignation(designation: any){
    return this.httpClient.put(this.baseUrl + '/employee/updateDesignation', designation);
  }
  deleteDesignation(designation_id: any){
    return this.httpClient.post(this.baseUrl + '/employee/deleteDesignation', designation_id);
  }

  //clusters
  getAllClusters(){
    return this.httpClient.get(this.baseUrl + '/employee/getClusters');
  }
  createClusters(clusters: any){
    return this.httpClient.post(this.baseUrl + '/employee/createClusters', clusters);
  }
  updateCluster(cluster: any){
    return this.httpClient.put(this.baseUrl + '/employee/updateCluster', cluster);
  }
  deleteCluster(cluster_id: any){
    return this.httpClient.post(this.baseUrl + '/employee/deleteCluster', cluster_id);
  }

  //time tracker
  timein(time: any){
    return this.httpClient.post(this.baseUrl + '/time/timein', time);
  }
  updateTime(time: any){
    return this.httpClient.put(this.baseUrl + '/time/updateTime', time);
  }
  overtime(time: any){
    return this.httpClient.put(this.baseUrl + '/time/overtime', time);
  }
}
