import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { AuthService } from 'app/service/auth_service/auth.service';
import { DataService } from 'app/service/data.service';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate{

  constructor(
    private _authService: AuthService,
    private _dataService: DataService,
    private _router: Router,
    private _cookieService: CookieService,
  ){}

  canActivate(): boolean{
    if(this._authService.loggedIn()){
      let isAdmin = this._cookieService.get("isAdmin").toString();

      if(isAdmin == 'true'){
        return true;
      } 

      this._router.navigateByUrl('/about-us');
      return false;
      
    }
    else{
      this._router.navigateByUrl('/login');
      return false;
    }
  }
}
