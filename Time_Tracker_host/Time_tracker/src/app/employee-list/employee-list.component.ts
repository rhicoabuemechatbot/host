import { DatePipe } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataService } from 'app/service/data.service';
import { CookieService } from 'ngx-cookie-service';
import { AddEmployeeDialogComponent } from './add-employee-dialog/add-employee-dialog.component';
import { DeleteEmployeeDialogComponent } from './delete-employee-dialog/delete-employee-dialog.component';
import { DownloadEmployeePdfComponent } from './download-employee-pdf/download-employee-pdf.component';
import { UpdateEmployeeDialogComponent } from './update-employee-dialog/update-employee-dialog.component';

export interface EmployeeData {
  employee: Employee;
}

export interface EmployeeDataPDF {
  employee: EmployeePDF;
}

class Employee {
  id: String;
  
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  designation_name: String;
  cluster_name: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  work_mode: String;
  address: String;
  birthdate: any;
  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

class TimeTrack{
  id: String;
  name: String;
  employee_id: String;
  morning_time_in: String;
  updated_time: String;
  work_mode: String;
}

class EmployeePDF{
  id: String;
  employee_id: String;
  
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  designation_name: String;
  cluster_name: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  work_mode: String;
  address: String;
  birthdate: any;
  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;

  monthPDF: String;
  monthStringPDF: String;
  dayPDF: String;
}

@Component({
  selector: 'employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employeesList: any;

  employee = new Employee();
  currentUser = new Employee();
  employeePDF = new EmployeePDF();

  timetrack = new TimeTrack();

  constructor(
    private _dataService: DataService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    public datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.getInitialEmployees();
    this.getCurrentUser();
  }

  getInitialEmployees(){
    this._dataService.getEmployees()
      .subscribe((res) => {
        this.employeesList = res;
    });
  }

  getCurrentUser(){
    this._dataService.getCurrentUser()
      .subscribe((res) => {
        this.currentUser = res['employee'];
        this.currentUser.name = this.currentUser.firstName + " " + this.currentUser.lastName;
        console.log(res);
    }); 
  }

  //CRUD employee
  addEmployee(): void {
    const dialogRef = this.dialog.open(AddEmployeeDialogComponent, {
      width: '1000px',
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.submitClicked.subscribe(result => {
        this._dataService.createEmployees(result)
          .subscribe((res) => {
            console.log(res);
            
            this.openSnackBar("Success! Completely Registered");
            this.getInitialEmployees();
        }, (error) => {
          let error_message = error['error']['message'];
          console.log(error_message);

          this.openSnackBar(error_message);
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  updateEmployee(employee: Employee): void {
    const dialogRef = this.dialog.open(UpdateEmployeeDialogComponent, {
      width: '1000px',
      data: {employee: employee}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.updateClicked.subscribe(result => {
        this._dataService.updateEmployee(result)
          .subscribe((res) => {
            console.log(res);
            this.openSnackBar('Successfully updated user!');
          }, (error) => {
            console.log(error);
            this.openSnackBar(error);
          });
        dialogSubmitSubscription.unsubscribe();
    });
  } 
  deleteEmployee(id: Employee){
    const dialogRef = this.dialog.open(DeleteEmployeeDialogComponent, {
      width: '500px',
      data: {employee: id}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.deleteClicked.subscribe(result => {
        
        this._dataService.deleteEmployee(result)
          .subscribe((res) => {
            console.log(res);
            this.openSnackBar("Employee successfully deleted!");
            this.getInitialEmployees();
        }, (error) => {
          this.openSnackBar(error);
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }

  //use this for downloading time track pdf
  downloadPDF(employee: EmployeePDF){
    employee.name = employee.firstName + " " + employee.lastName;
  
    const dialogRef = this.dialog.open(DownloadEmployeePdfComponent, {
      width: '500px',
      data: {employee: employee}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.downloadPDF.subscribe(result => {
        console.log(result);

        this._dataService.downloadPDF(result)
          .subscribe((res) => {
            this.saveAsBlob(res);
            console.log(res);
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }

  //time in functionality
  createTime(){
    var now = new Date();
    let latest_date =this.datepipe.transform(now, 'YYYY-MM-dd hh:mm:ss');

    this.timetrack.employee_id = this.currentUser.id;
    this.timetrack.name = this.currentUser.firstName + " " + this.currentUser.lastName;
    this.timetrack.morning_time_in = latest_date;
    this.timetrack.work_mode = this.currentUser.work_mode;

    console.log(this.timetrack );

    this._dataService.timein(this.timetrack)
      .subscribe((res) => {
        this.timetrack.id = res['id'];
        console.log(res);
      });
  }
  updateTime(){
    var now = new Date();
    let latest_date = this.datepipe.transform(now, 'YYYY-MM-dd HH:mm:ss');

    console.log(latest_date);
    this.timetrack.updated_time = latest_date;  

    this._dataService.updateTime(this.timetrack)
      .subscribe((res) => {
        console.log(res);
    });
  }
  overtime(){
    var now = new Date();
    let latest_date =this.datepipe.transform(now, 'YYYY-MM-dd hh:mm:ss');

    this.timetrack.updated_time = latest_date;

    this._dataService.overtime(this.timetrack)
      .subscribe((res) => {
        console.log(res);
    });
  }

  saveAsBlob(data: any){
    var blob = new Blob([data], { type: 'application/pdf' });
    var url= window.URL.createObjectURL(blob);
    window.open(url);
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "Close", { duration: 2000});
  }
}
