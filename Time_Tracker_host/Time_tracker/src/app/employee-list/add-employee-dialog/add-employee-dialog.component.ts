import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { DataService } from 'app/service/data.service';

class Employee {
  employee_id: String;
  
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  address: String;
  birthdate: any;

  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

@Component({
  selector: 'add-employee-dialog',
  templateUrl: './add-employee-dialog.component.html',
  styleUrls: ['./add-employee-dialog.component.css']
})

export class AddEmployeeDialogComponent implements OnInit {

  @Output() submitClicked = new EventEmitter<any>();

  employee = new Employee();

  suffixNames = ["N/A", "Jr.", "Sr.", "I", "II", "III", "IV", "V"];
  employment_status = ["Job Order", "Regular", "Contractual"];

  email = new FormControl('', [Validators.required, Validators.email]);

  designations: any;
  clusters: any;

  birthdate: any;
  
  constructor(
    public dialogRef: MatDialogRef<AddEmployeeDialogComponent>,
    public _dataService: DataService
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
    this._dataService.getAllDesignations()
      .subscribe((res) => {
        this.designations = res;
    })
    
    this._dataService.getAllClusters()
      .subscribe((res) => {
        this.clusters = res;
    })
  }

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  submit(){
    this.employee.name = this.employee.firstName + " " + this.employee.lastName;
    this.employee.birthdate = this.birthdate['singleDate']['formatted'];
    
    console.log(this.employee.username);
    
    this.submitClicked.emit(this.employee);
    this.dialogRef.close();
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
}
