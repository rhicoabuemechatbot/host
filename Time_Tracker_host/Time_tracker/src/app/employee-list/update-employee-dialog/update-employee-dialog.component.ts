import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from 'app/service/data.service';
import { data } from 'jquery';
import { EmployeeData } from '../employee-list.component';

class Employee {
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  address: String;
  birthdate: any;
  work_mode: String;

  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

@Component({
  selector: 'update-employee-dialog',
  templateUrl: './update-employee-dialog.component.html',
  styleUrls: ['./update-employee-dialog.component.css']
})
export class UpdateEmployeeDialogComponent implements OnInit {

  @Output() updateClicked = new EventEmitter<any>();

  suffixNames = ["N/A", "Jr.", "Sr.", "I", "II", "III", "IV", "V"];
  employment_status = ["Job Order", "Regular", "Contractual"];
  work_mode_list = ["On Premise", "Work from Home", "Official Business"];

  email = new FormControl('', [Validators.required, Validators.email]);

  employee = new Employee();

  designations: any;
  clusters: any;

  birthdate: any;
  
  constructor(
    public dialogRef: MatDialogRef<UpdateEmployeeDialogComponent>,
    public _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: EmployeeData
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
    console.log(this.data);
    this.employee = this.data.employee;
    this.birthdate = this.employee.birthdate;

    this._dataService.getAllDesignations()
      .subscribe((res) => {
        this.designations = res;
    })
    
    this._dataService.getAllClusters()
      .subscribe((res) => {
        this.clusters = res;
    })
  }

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  update(){
    this.employee.name = this.employee.firstName + " " + this.employee.lastName;
    this.employee.birthdate = this.birthdate;
    
    this.updateClicked.emit(this.employee);
    this.dialogRef.close();
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }

}
