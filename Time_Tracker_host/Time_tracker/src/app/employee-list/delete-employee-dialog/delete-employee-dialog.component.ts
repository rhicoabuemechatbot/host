import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from 'app/service/data.service';
import { EmployeeData } from '../employee-list.component';

class Employee {
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  address: String;
  birthdate: any;
  work_mode: String;

  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

@Component({
  selector: 'delete-employee-dialog',
  templateUrl: './delete-employee-dialog.component.html',
  styleUrls: ['./delete-employee-dialog.component.css']
})
export class DeleteEmployeeDialogComponent implements OnInit {

  @Output() deleteClicked = new EventEmitter<any>();
  
  employee = new Employee();

  constructor(
    public dialogRef: MatDialogRef<DeleteEmployeeDialogComponent>,
    public _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: EmployeeData
  ) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
    this.employee = this.data.employee;
    this.employee.name = this.data.employee.firstName + " " + this.data.employee.lastName;
  }

  onNoClick(){
    this.dialogRef.close();
  }

  delete(){
    this.employee.name = this.employee.firstName + " " + this.employee.lastName;
    
    this.deleteClicked.emit(this.employee);
    this.dialogRef.close();
  }

}
