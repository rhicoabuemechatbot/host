import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadEmployeePdfComponent } from './download-employee-pdf.component';

describe('DownloadEmployeePdfComponent', () => {
  let component: DownloadEmployeePdfComponent;
  let fixture: ComponentFixture<DownloadEmployeePdfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadEmployeePdfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadEmployeePdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
