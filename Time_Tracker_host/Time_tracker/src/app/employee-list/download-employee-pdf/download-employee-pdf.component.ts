import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from 'app/service/data.service';
import { EmployeeDataPDF } from '../employee-list.component';

class EmployeePDF {
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  address: String;
  birthdate: any;
  work_mode: String;

  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;

  monthPDF: String;
  monthStringPDF: String;
  dayPDF: String;
}

@Component({
  selector: 'download-employee-pdf',
  templateUrl: './download-employee-pdf.component.html',
  styleUrls: ['./download-employee-pdf.component.css']
})

export class DownloadEmployeePdfComponent implements OnInit {

  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  @Output() downloadPDF = new EventEmitter<any>();
  
  employee = new EmployeePDF();

  constructor(
    public dialogRef: MatDialogRef<DownloadEmployeePdfComponent>,
    public _dataService: DataService,
    @Inject(MAT_DIALOG_DATA) public data: EmployeeDataPDF
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
    this.employee = this.data.employee;
  }

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  submit(){
    this.employee.name = this.employee.firstName + " " + this.employee.lastName;
    this.employee.monthStringPDF = this.months[Number(this.employee.monthPDF)-1];
    
    this.downloadPDF.emit(this.employee);
    this.dialogRef.close();
  }
}
