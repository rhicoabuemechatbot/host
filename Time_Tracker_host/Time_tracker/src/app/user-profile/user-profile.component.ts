import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/service/data.service';

class Employee {
  id: String;
  
  employee_id: String;
  firstName: String;
  middleName: String;
  lastName: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  designation_name: String;
  cluster_name: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  work_mode: String;
  address: String;
  birthdate: any;
  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

class User{
  name: String;
  email: String;
  username: String;
}

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  currentEmployee: any;
  currentUser = new User();

  constructor(
    private _dataService: DataService,
  ) { }

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser(){
    this._dataService.getCurrentUser()
      .subscribe((res) => {
        let employee = res['employee'];
        let user = res['user'];

        this.currentEmployee = employee;
        this.currentUser = user;
      });
  }

  print(){
    console.log(this.currentEmployee);
  }
}
