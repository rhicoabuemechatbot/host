import { StringMapWithRename } from '@angular/compiler/src/compiler_facade_interface';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { DataService } from 'app/service/data.service';

class Employee {
  firstName: String;
  middleName: String;
  lastName: String;
  employee_id: String;
  suffix: String;
  designation_id: String;
  clusters_id: String;
  cellphone_number: String;
  employment_status: String;
  activation_status: String;
  address: String;
  birthdate: any;

  email: String;
  name: String;
  username:String;
  password: String;
  password_confirmation: String;
}

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  suffixNames = ["N/A", "Jr.", "Sr.", "I", "II", "III", "IV", "V"];
  employment_status = ["Job Order", "Regular", "Contractual"];

  email = new FormControl('', [Validators.required, Validators.email]);

  designations: any;
  clusters: any;

  birthdate: any;

  employee = new Employee();

  constructor(
    private _dataService: DataService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void {
    this._dataService.getAllDesignations()
      .subscribe((res) => {
        this.designations = res;
    })
    
    this._dataService.getAllClusters()
      .subscribe((res) => {
        this.clusters = res;
    })
  }

  save(){
    this.employee.name = this.employee.firstName + " " + this.employee.lastName;
    this.employee.birthdate = this.birthdate['singleDate']['formatted'];

    this._dataService.createEmployees(this.employee)
      .subscribe((res) => {
        this.openSnackBar("Success! Completely Registered");
        this.router.navigateByUrl('/login');
    }, (error) => {
      console.log(error);
      let error_message = error['error']['message'];
      this.openSnackBar(error_message);
    });
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, "Close", { duration: 2000});
  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'You must enter a value';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }
}
