import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/service/data.service';

@Component({
  selector: 'app-icons',
  templateUrl: './icons.component.html',
  styleUrls: ['./icons.component.css']
})
export class IconsComponent implements OnInit {
  employees: any;
  designations: any;
  clusters: any;

  constructor(private _dataService: DataService) { }

  ngOnInit() {
    this.getInitialData();
  }

  getInitialData(){
    this._dataService.getAllDesignations()
      .subscribe((res) => {
        this.designations = res;
    });

    this._dataService.getAllClusters()
      .subscribe((res)=>{
        console.log(res);
        this.clusters = res;
      }); 
  }
}
