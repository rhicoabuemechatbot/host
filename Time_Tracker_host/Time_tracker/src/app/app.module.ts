import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { UpgradeComponent } from './upgrade/upgrade.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { RegisterComponent } from './register/register.component';
import { MatSelectModule } from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { DataService } from './service/data.service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './service/auth_service/auth.service';
import { AuthGuard } from './auth_guard/auth.guard';
import { AngularMyDatePickerModule } from 'angular-mydatepicker';
import {MatIconModule} from '@angular/material/icon';
import { ReferencesComponent } from './references/references.component';
import {MatDialogModule} from '@angular/material/dialog';
import { AddDesignationDialogComponent } from './references/add-designation-dialog/add-designation-dialog.component';
import { AddClusterDialogComponent } from './references/add-cluster-dialog/add-cluster-dialog.component';
import { UpdateDesignationDialogComponent } from './references/update-designation-dialog/update-designation-dialog.component';
import { UpdateClusterDialogComponent } from './references/update-cluster-dialog/update-cluster-dialog.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { AddEmployeeDialogComponent } from './employee-list/add-employee-dialog/add-employee-dialog.component';
import { UpdateEmployeeDialogComponent } from './employee-list/update-employee-dialog/update-employee-dialog.component';
import { DatePipe } from '@angular/common';
import { DownloadEmployeePdfComponent } from './employee-list/download-employee-pdf/download-employee-pdf.component';
import { DeleteEmployeeDialogComponent } from './employee-list/delete-employee-dialog/delete-employee-dialog.component';
import { EmployeePdfComponent } from './employee-pdf/employee-pdf.component';
import { DownloadBulkPdfComponent } from './employee-pdf/download-bulk-pdf/download-bulk-pdf.component';
import { AboutUsComponent } from './about-us/about-us.component'

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
    MatRadioModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    AngularMyDatePickerModule,
    MatIconModule,
    MatDialogModule
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    RegisterComponent,
    ReferencesComponent,
    AddDesignationDialogComponent,
    AddClusterDialogComponent,
    UpdateDesignationDialogComponent,
    UpdateClusterDialogComponent,
    EmployeeListComponent,
    AddEmployeeDialogComponent,
    UpdateEmployeeDialogComponent,
    DownloadEmployeePdfComponent,
    DeleteEmployeeDialogComponent,
    EmployeePdfComponent,
    DownloadBulkPdfComponent,
    AboutUsComponent,

  ],
  providers: [
    DataService,
    CookieService,
    AuthService,
    AuthGuard,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
