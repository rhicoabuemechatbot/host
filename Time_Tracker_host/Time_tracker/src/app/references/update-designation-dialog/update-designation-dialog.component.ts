import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DesignationData } from '../references.component';

@Component({
  selector: 'update-designation-dialog',
  templateUrl: './update-designation-dialog.component.html',
  styleUrls: ['./update-designation-dialog.component.css']
})
export class UpdateDesignationDialogComponent implements OnInit {

  @Output() updateClicked = new EventEmitter<any>();

  constructor(
    public dialogRef: MatDialogRef<UpdateDesignationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DesignationData
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
  }

  addedDesignation: String;

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  update(){
    this.updateClicked.emit(this.addedDesignation);
    this.dialogRef.close();
  }

}
