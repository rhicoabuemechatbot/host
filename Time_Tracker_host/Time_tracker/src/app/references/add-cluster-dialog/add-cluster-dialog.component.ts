import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'add-cluster-dialog',
  templateUrl: './add-cluster-dialog.component.html',
  styleUrls: ['./add-cluster-dialog.component.css']
})
export class AddClusterDialogComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();

  constructor(
    public dialogRef: MatDialogRef<AddClusterDialogComponent>
  ) { 
    dialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

  addedCluster: String;

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  submit(){
    this.submitClicked.emit(this.addedCluster);
    this.dialogRef.close();
  }
}
