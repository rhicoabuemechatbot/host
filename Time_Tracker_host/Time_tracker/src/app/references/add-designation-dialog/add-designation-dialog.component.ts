import { Component, Input, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'add-designation-dialog',
  templateUrl: './add-designation-dialog.component.html',
  styleUrls: ['./add-designation-dialog.component.css']
})
export class AddDesignationDialogComponent implements OnInit {
  @Output() submitClicked = new EventEmitter<any>();

  constructor(
    public dialogRef: MatDialogRef<AddDesignationDialogComponent>
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
  }

  addedDesignation: String;

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  submit(){
    this.submitClicked.emit(this.addedDesignation);
    this.dialogRef.close();
  }
}
