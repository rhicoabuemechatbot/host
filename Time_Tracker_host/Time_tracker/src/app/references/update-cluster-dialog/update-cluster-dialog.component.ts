import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ClustersData } from '../references.component';

@Component({
  selector: 'update-cluster-dialog',
  templateUrl: './update-cluster-dialog.component.html',
  styleUrls: ['./update-cluster-dialog.component.css']
})
export class UpdateClusterDialogComponent implements OnInit {
  @Output() updateClicked = new EventEmitter<any>();

  constructor(
    public dialogRef: MatDialogRef<UpdateClusterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ClustersData
  ) {
    dialogRef.disableClose = true;
   }

  ngOnInit(): void {
  }

  updatedClusters: String;

  @Output() event = new EventEmitter<string>()

  onNoClick(){
    this.dialogRef.close();
  }

  update(){
    this.updateClicked.emit(this.updatedClusters);
    this.dialogRef.close();
  }
}
