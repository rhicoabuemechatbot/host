import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateClusterDialogComponent } from './update-cluster-dialog.component';

describe('UpdateClusterDialogComponent', () => {
  let component: UpdateClusterDialogComponent;
  let fixture: ComponentFixture<UpdateClusterDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateClusterDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateClusterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
