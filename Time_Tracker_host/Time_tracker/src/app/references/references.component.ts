import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DataService } from 'app/service/data.service';
import { AddClusterDialogComponent } from './add-cluster-dialog/add-cluster-dialog.component';
import { AddDesignationDialogComponent } from './add-designation-dialog/add-designation-dialog.component';
import { UpdateClusterDialogComponent } from './update-cluster-dialog/update-cluster-dialog.component';
import { UpdateDesignationDialogComponent } from './update-designation-dialog/update-designation-dialog.component';

class Designation{
  id: String;
  designation: String;
}

class Clusters{
  id: String;
  clusters: String;
}

export interface DesignationData {
  designation: string;
}

export interface ClustersData {
  clusters: string;
}

declare var $: any;

@Component({
  selector: 'references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.css']
})
export class ReferencesComponent implements OnInit {
  employees: any;
  designationsList: any;
  clustersList: any;

  designation = new Designation();
  clusters = new Clusters();

  constructor(
    private _dataService: DataService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getInitialData();
  }

  //designations
  addDesignation(): void {
    const dialogRef = this.dialog.open(AddDesignationDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.submitClicked.subscribe(result => {
        this.designation.designation = result;

        this._dataService.createDesignations(this.designation)
        .subscribe((res) => {
          this.getInitialData();
          this.showNotification('success', 'Successfully added Designation!');
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  updateDesignation(id: String, designation: String): void {
    this.designation.id = id;
    this.designation.designation = designation;

    const dialogRef = this.dialog.open(UpdateDesignationDialogComponent, {
      width: '500px',
      data: {designation: this.designation.designation}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.updateClicked.subscribe(result => {
        this.designation.designation = result;

        this._dataService.updateDesignation(this.designation)
          .subscribe((res) => {
            this.getInitialData();
            this.showNotification('success', 'Successfully updated Designation!');
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  deleteDesignation(id: String): void {
    this.designation.id = id;

    this._dataService.deleteDesignation(this.designation)
      .subscribe((res) => {
        this.getInitialData();
        this.showNotification('success', 'Successfully deleted Designation!');
    });
  }
  

  //clusters
  addClusters(): void {
    const dialogRef = this.dialog.open(AddClusterDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.submitClicked.subscribe(result => {
        this.clusters.clusters = result;

        this._dataService.createClusters(this.clusters)
          .subscribe((res) => {
            this.getInitialData();
            this.showNotification('success', 'Successfully added Cluster!');
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  updateClusters(id: String, clusters: String): void {
    this.clusters.id = id;
    this.clusters.clusters = clusters;

    const dialogRef = this.dialog.open(UpdateClusterDialogComponent, {
      width: '500px',
      data: {clusters: this.clusters.clusters}
    });

    dialogRef.afterClosed().subscribe(result => {});

    const dialogSubmitSubscription = 
      dialogRef.componentInstance.updateClicked.subscribe(result => {
        this.clusters.clusters = result;

        this._dataService.updateCluster(this.clusters)
          .subscribe((res) => {
            this.getInitialData();
            this.showNotification('warning', 'Successfully updated Cluster!');
        });

        dialogSubmitSubscription.unsubscribe();
    });
  }
  deleteClusters(id: String): void {
    this.clusters.id = id;

    this._dataService.deleteCluster(this.clusters)
      .subscribe((res) => {
        this.getInitialData();
        this.showNotification('danger', 'Successfully deleted Cluster!');
    });
  }

  //get initial data upon loading
  getInitialData(){
    this._dataService.getAllDesignations()
      .subscribe((res) => {
        this.designationsList = res;
    });

    this._dataService.getAllClusters()
      .subscribe((res)=>{
        this.clustersList = res;
      }); 
  }

  //show notification
  showNotification(type: String, message: any){
    $.notify({
        icon: "notifications",
        message: message

    },{
        type: type,
        timer: 500,
        placement: {
            from: 'bottom',
            align: 'right'
        },
        template: '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
  }
}
